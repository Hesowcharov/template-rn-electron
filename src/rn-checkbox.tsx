import React from 'react'
import { Button } from 'react-native'

interface RNCheckboxProps {
  active: boolean
  onPress?: () => void
}

export const RNCheckbox = ({ active, onPress }: RNCheckboxProps) => {
  return (
    <Button title='button' color={active ? 'green' : 'gray'} onPress={onPress} />
  )
}

import React, { PropsWithChildren, useCallback, useRef } from 'react';
import { Animated, GestureResponderEvent, TouchableOpacity, TouchableOpacityProps } from 'react-native';

type Props = TouchableOpacityProps & {
  squeezable?: boolean;
};

const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity);

export const Pressable = ({ children, onPressIn, onPressOut, squeezable, ...props }: PropsWithChildren<Props>) => {
  const squeezeAnimation = useRef(new Animated.Value(0)).current;
  const activeOpacity = squeezable ? 1 : 0.4;

  const startSqueezeAnimation = useCallback(() => {
    if (squeezable) {
      squeezeAnimation.removeAllListeners();
      Animated.timing(squeezeAnimation, {
        toValue: 1,
        duration: 100,
        useNativeDriver: true,
      }).start();
    }
  }, [squeezeAnimation, squeezable]);

  const stopSqueezeAnimation = useCallback(() => {
    if (squeezable) {
      squeezeAnimation.removeAllListeners();
      Animated.timing(squeezeAnimation, {
        toValue: 0,
        duration: 100,
        useNativeDriver: true,
      }).start();
    }
  }, [squeezeAnimation, squeezable]);

  const onPressInCallback = useCallback(
    (event: GestureResponderEvent) => {
      startSqueezeAnimation();
      onPressIn?.(event);
    },
    [startSqueezeAnimation, onPressIn],
  );

  const onPressOutCallback = useCallback(
    (event: GestureResponderEvent) => {
      stopSqueezeAnimation();
      onPressOut?.(event);
    },
    [stopSqueezeAnimation, onPressOut],
  );

  const squeezeStyle = {
    transform: [
      {
        scale: squeezeAnimation.interpolate({
          inputRange: [0, 1],
          outputRange: [1, 0.95],
        }),
      },
    ],
  };

  return (
    <AnimatedTouchable
      {...props}
      style={squeezeStyle}
      activeOpacity={activeOpacity}
      onPressIn={onPressInCallback}
      onPressOut={onPressOutCallback}
    >
      {children}
    </AnimatedTouchable>
  );
};

import React, { useState } from 'react'
import { Pressable } from './rn-component'
import { Text, View, Image } from 'react-native'
import { RNCheckbox } from './rn-checkbox'
// @ts-ignore
import { Demo } from './demo'

export const TestFeatures = () => {
  const [toggle, setToggle] = useState(true)

  const onPressButton = () => {
    setToggle(f => !f)
  }

  return (
    <div>
      <h2>
        Hello from React zz aa!
      </h2>
      <Pressable>
        <Text> hello from RN! What's up! ddasdas d</Text>
      </Pressable>
      <View style={{marginTop: 20}} />
      <RNCheckbox active={toggle} onPress={onPressButton} />
      <Demo />
      <Image style={{ width: '100%', height: 500, }} source={require('./assets/tinkoff-logo.png')} />
    </div>
  )
}

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { TestFeatures } from './test-features';

function render() {
  ReactDOM.render(
    <TestFeatures />,
    document.body);
}

render();
